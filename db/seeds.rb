# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create(name: "User Bot", score:0)
User.create(name: "John Doe", score:7)
User.create(name: "Jane Doe", score:12)
User.create(name: "Janette Doeh", score:17)
User.create(name: "Ioana Porumbel", score:21)