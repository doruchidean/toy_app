module ApplicationHelper

	def full_title(provided="")
		msg = "title " + provided;
		flash.now[:notice] = msg;
		if provided.empty?
			title = "ClujPoker League";
		else
			title =  "ClujPoker League | " +provided;
		end
	end
end
