class StaticPagesController < ApplicationController
  def home
  	@users = User.all.order(score: :desc);
  	@leader = @users[0];
  end

  def help
  end

  def about
  end

end
